﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PricingAPI.Models;

namespace PricingAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CostController : ControllerBase
    {
        private readonly ILogger<CostController> _logger;
        private SqlConnectionStringBuilder _builder;
        private SqlConnection _conn;

        public CostController(ILogger<CostController> logger)
        {
            _logger = logger;

            // Setup SqlConnection
            _builder = new SqlConnectionStringBuilder
            {
                DataSource = "y06ore1yfb.database.windows.net",
                UserID = "svc_PW",
                Password = "Pricing@Pariveda",
                InitialCatalog = "PricingWorksheet"
            };
        }

        /// <summary>
        /// GET that returns all rows in the CostRates table.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<CostLine> Get()
        {
            var costs = new List<CostLine>();

            try
            {
                using(_conn = new SqlConnection(_builder.ConnectionString))
                {
                    var sb = new StringBuilder();
                    sb.Append(
                        "SELECT [Level], [Office], [Cost] " +
                        "FROM [dbo].[CostRates]");

                    var sql = sb.ToString();

                    using var command = new SqlCommand(sql, _conn);
                    _conn.Open();

                    using var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        costs.Add(new CostLine
                        {
                            Level = reader.GetString(0),
                            Office = reader.GetString(1),
                            Cost = reader.GetDouble(2),
                        });
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            _conn.Close();
            return costs;
        }
    }
}
