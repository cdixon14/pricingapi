﻿namespace PricingAPI.Models
{
    public class CostLine
    {
        public string Level { get; set; }
        public string Office { get; set; }
        public double Cost { get; set; }
    }
}